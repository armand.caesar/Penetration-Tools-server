package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

//Payload model
type Payload struct {
	URL string `json:"url"`
}

func main() {
	r := gin.Default()
	r.Use(cors.Default())

	r.Use(static.Serve("/", static.LocalFile("./public", true)))

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "free platform alive!", "serverTime": time.Now().UTC()})
	})

	r.POST("/proxy", func(c *gin.Context) {
		var payload Payload
		if err := c.ShouldBindWith(&payload, binding.JSON); err != nil {
			fmt.Printf("Binding Error")
			c.JSON(500, gin.H{
				"message": "Binding Error",
			})
		}

		response, err := http.Get(payload.URL)
		fmt.Println(payload.URL)

		if err != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err)
		} else {
			data, _ := ioutil.ReadAll(response.Body)

			str := string(data[:])

			c.JSON(200, gin.H{
				"message": str,
			})
		}
	})
	r.Run(":9000") // listen and serve on 0.0.0.0:8080
}
